from django.urls import path
from recipes_app.views import show_recipe, recipe_list

urlpatterns = [
    path("recipes/", recipe_list, name="recipe_list"),
    path("recipes/<int:id>/", show_recipe, name="show-recipe")
]

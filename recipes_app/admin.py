from django.contrib import admin
from recipes_app.models import Recipe


@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'title',
        'description',
        'created_on'
    ]
